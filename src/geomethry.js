export const sqrt = Math.sqrt;

/**
    Geron expression to evaluate triangle square
 */
export function getTriangleSquare(a, b, c) {
    let p = 0.5 * (a + b + c);

    return sqrt(p * (p-a) * (p-b) * (p-c));
}