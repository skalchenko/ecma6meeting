## Hi, this repo will be used in dev meeting for introducing to ES2015/2017

To start working you should use following commands:

`npm install` - use this to build the project

`npm run serve` - next step -> run the task 'serve' to initialize and run an application

Files will be compiled to es5 and served by nodemon server.
